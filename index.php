<?php 

require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");
$sheep->name = "Shaun";
$sheep->legs = "2";
$sheep->cold_blooded = "false";

echo $sheep->name; 
echo "<br>";// "shaun"
echo $sheep->legs;
echo "<br>";  // 2
echo $sheep->cold_blooded;
echo "<br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())



$sungokong = new Ape("kera sakti");
$sungokong->yell();
echo "<br>";
echo $sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ;
echo "<br>";
echo $kodok->jump(); // "hop hop"
?>